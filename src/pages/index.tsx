import HeaderComponent from "@/components/header";
import Screen from "@/components/screen";

export default function Page() {
  return (
    <>
      <Screen
        titleHeader="Home Page"
        header={<HeaderComponent></HeaderComponent>}
        content={<></>}
      ></Screen>
    </>
  );
}
