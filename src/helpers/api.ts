import axios from "axios";

const baseUrl = "http://localhost:3030";
// const baseUrl = "http://20.255.56.9:3030";

export default function callApi({
  endpoint,
  method = "GET" || "POST" || "PATCH" || "DELETE",
  body,
}: {
  endpoint: string;
  method: any;
  body?: any;
}) {
  const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
  };

  const instance = axios.create({ headers });

  instance.interceptors.request.use(
    (config) => {
      const token = localStorage.getItem("accessToken");
      if (token) {
        config.headers["Authorization"] = `Bearer ${token}`;
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    (response) => {
      return response;
    },
    async (error) => {
      const originalConfig = error.config;
      if (error.response && error.response.status === 401) {
        try {
          console.log("call refresh token api");
          const result = await instance.post(
            `${baseUrl}/accounts/refresh-token`,
            {
              refreshToken: localStorage.getItem("refreshToken"),
            }
          );
          const { accessToken } = result.data.data;
          localStorage.setItem("accessToken", accessToken);
          originalConfig.headers["Authorization"] = `Bearer ${accessToken}`;

          return instance(originalConfig);
        } catch (err: any) {
          if (err.response && err.response.status === 400) {
            localStorage.removeItem("id");
            localStorage.removeItem("email");
            localStorage.removeItem("refreshToken");
            localStorage.removeItem("accessToken");
            window.location.href = "/sign-in";
          }
          return Promise.reject(err);
        }
      }
      return Promise.reject(error);
    }
  );

  return instance.request({
    method: method,
    url: `${baseUrl}/${endpoint}`,
    data: body,
    responseType: "json",
  });
}
