import callApi from "@/helpers/api";
import { AppBar, Box, Button, Toolbar, Typography } from "@mui/material";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";

export default function HeaderComponent() {
  const [email, setEmail] = useState<any>("");
  const router = useRouter();

  useEffect(() => {
    if (localStorage.getItem("email")) {
      // console.log("OK");
      const emailS = localStorage.getItem("email");
      setEmail(emailS);
    } else {
      setEmail("");
    }
  }, []);

  const handleNextSignIn = () => {
    router.push("/sign-in");
  };

  const handleLogout = () => {
    callApi({
      endpoint: `accounts/${localStorage.getItem("id")}`,
      method: "GET",
    })
      .then((res) => {
        toast.success("Logout Success!!!", {
          autoClose: 1000,
          position: toast.POSITION.TOP_RIGHT,
        });
        localStorage.removeItem("id");
        localStorage.removeItem("email");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("accessToken");
        window.location.href = "/sign-in";
      })
      .catch((err: any) => {
        console.log(err);
      });
  };

  return (
    <>
      <ToastContainer></ToastContainer>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
      >
        <Toolbar sx={{ flexWrap: "wrap" }}>
          <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            Test
          </Typography>

          {email ? (
            <Box display={"flex"} alignItems={"center"}>
              <Typography>Hello! {email}</Typography>
              <Button
                variant="outlined"
                sx={{ my: 1, mx: 1.5 }}
                onClick={handleLogout}
              >
                Logout
              </Button>
            </Box>
          ) : (
            <Button
              variant="outlined"
              sx={{ my: 1, mx: 1.5 }}
              onClick={handleNextSignIn}
            >
              Login
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </>
  );
}
