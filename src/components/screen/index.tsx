import { Box } from "@mui/material";
import Head from "next/head";
import React from "react";
import isEqual from "react-fast-compare";

export default React.memo(function ScreenComponent(props: {
  titleHeader?: string;
  header?: React.JSX.Element;
  content?: React.JSX.Element;
}) {
  return (
    <>
      <Head>
        <title>{props.titleHeader}</title>
      </Head>

      <Box>
        {props.header}
        {props.content}
      </Box>
    </>
  );
},
isEqual);
