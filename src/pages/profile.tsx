import HeaderComponent from "@/components/header";
import Screen from "@/components/screen";
import callApi from "@/helpers/api";
import {
  Box,
  Button,
  Container,
  CssBaseline,
  Grid,
  TextField,
  ThemeProvider,
  Typography,
  createTheme,
} from "@mui/material";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import * as yup from "yup";

const validationSchema = yup.object({
  fullname: yup.string().required("Fullname is required"),
  age: yup.number().required("Age is required"),
});

const defaultTheme = createTheme();

export default function Profile() {
  const [state, setState] = useState(false);
  const [userInfoByAccId, setUserInfoByAccId] = useState<{
    email: string;
    fullname: string;
    age: string;
  }>({
    email: "",
    fullname: "",
    age: "",
  });

  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      fullname: "Lorem ...",
      age: 18,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  const getUserInfoByAccId = () => {
    callApi({
      endpoint: `users/${localStorage.getItem("id")}`,
      method: "GET",
    })
      .then((res) => {
        // setState(res.data.data[0].fullname);
        setState(true);
        setUserInfoByAccId({
          email: res.data.data[0].accountInfo.email,
          fullname: res.data.data[0].fullname,
          age: res.data.data[0].age,
        });
      })
      .catch((err: any) => {
        setState(false);
        console.log(err);
      });
  };

  useEffect(() => {
    getUserInfoByAccId();
  }, [state]);

  const handleCreateUser = () => {
    const body = {
      accountId: localStorage.getItem("id"),
      fullname: formik.values.fullname,
      age: formik.values.age,
    };
    callApi({
      endpoint: `users`,
      method: "POST",
      body: body,
    })
      .then((res) => {
        setState(true);
        toast.success("Update User Success!", {
          position: toast.POSITION.TOP_RIGHT,
          autoClose: 1000,
        });
      })
      .catch((err) => {
        setState(false);
        toast.error(err.response.data.message, {
          position: toast.POSITION.TOP_RIGHT,
          autoClose: 1000,
        });
      });
  };

  const handleUpdateUser = () => {
    router.push(
      `/update-profile?fullname=${userInfoByAccId.fullname}&age=${userInfoByAccId.age}`
    );
  };

  return (
    <>
      <Screen
        titleHeader="Profile"
        header={<HeaderComponent></HeaderComponent>}
        content={
          <>
            {state ? (
              <Box display={"flex"} justifyContent={"center"} mt={6}>
                <Box>
                  <Grid container>
                    <Grid item xs={12}>
                      <Typography>Email: {userInfoByAccId.email}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>Email: {userInfoByAccId.fullname}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography>Email: {userInfoByAccId.age}</Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <Button
                        type="submit"
                        fullWidth
                        variant="outlined"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleUpdateUser}
                      >
                        Update
                      </Button>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            ) : (
              <ThemeProvider theme={defaultTheme}>
                <ToastContainer></ToastContainer>
                <Container component="main" maxWidth="xs">
                  <CssBaseline />
                  <Box
                    sx={{
                      marginTop: 8,
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <Typography component="h1" variant="h5">
                      Thông tin User
                    </Typography>
                    <Box sx={{ mt: 1 }}>
                      <TextField
                        fullWidth
                        id="fullname"
                        name="fullname"
                        label="Fullname"
                        type="text"
                        value={formik.values.fullname}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={
                          formik.touched.fullname &&
                          Boolean(formik.errors.fullname)
                        }
                        helperText={
                          formik.touched.fullname && formik.errors.fullname
                        }
                      />
                      <Box sx={{ my: 3 }}></Box>
                      <TextField
                        fullWidth
                        id="age"
                        name="age"
                        label="Age"
                        type="text"
                        value={formik.values.age}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.age && Boolean(formik.errors.age)}
                        helperText={formik.touched.age && formik.errors.age}
                      />

                      <Button
                        type="submit"
                        fullWidth
                        variant="outlined"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleCreateUser}
                        disabled={
                          formik.errors.fullname || formik.errors.age
                            ? true
                            : false
                        }
                      >
                        Submit
                      </Button>
                    </Box>
                  </Box>
                </Container>
              </ThemeProvider>
            )}
          </>
        }
      ></Screen>
    </>
  );
}
