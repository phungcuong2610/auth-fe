import HeaderComponent from "@/components/header";
import Screen from "@/components/screen";
import callApi from "@/helpers/api";
import {
  Box,
  Button,
  Container,
  CssBaseline,
  TextField,
  ThemeProvider,
  Typography,
  createTheme,
} from "@mui/material";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import React from "react";
import { ToastContainer, toast } from "react-toastify";
import * as yup from "yup";

const validationSchema = yup.object({
  fullname: yup.string().required("Fullname is required"),
  age: yup.number().required("Age is required"),
});

const defaultTheme = createTheme();

export default function UpdateProfile() {
  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      fullname: router.query.fullname,
      age: router.query.age,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  const handleUpdateUser = () => {
    const body = {
      fullname: formik.values.fullname,
      age: formik.values.age,
    };
    callApi({
      endpoint: `users/${localStorage.getItem("id")}`,
      method: "PUT",
      body: body,
    })
      .then((res) => {
        toast.success("Update User Success!", {
          position: toast.POSITION.TOP_RIGHT,
          autoClose: 1000,
        });
        router.push("/profile");
      })
      .catch((err) => {
        toast.error(err.response.data.message, {
          position: toast.POSITION.TOP_RIGHT,
          autoClose: 1000,
        });
      });
  };

  return (
    <>
      <Screen
        titleHeader="Update User"
        header={<HeaderComponent></HeaderComponent>}
        content={
          <ThemeProvider theme={defaultTheme}>
            <ToastContainer></ToastContainer>
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <Box
                sx={{
                  marginTop: 8,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <Typography component="h1" variant="h5">
                  Thông tin User
                </Typography>
                <Box sx={{ mt: 1 }}>
                  <TextField
                    fullWidth
                    id="fullname"
                    name="fullname"
                    label="Fullname"
                    type="text"
                    value={formik.values.fullname}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={
                      formik.touched.fullname && Boolean(formik.errors.fullname)
                    }
                    helperText={
                      formik.touched.fullname && formik.errors.fullname
                    }
                  />
                  <Box sx={{ my: 3 }}></Box>
                  <TextField
                    fullWidth
                    id="age"
                    name="age"
                    label="Age"
                    type="text"
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.age && Boolean(formik.errors.age)}
                    helperText={formik.touched.age && formik.errors.age}
                  />

                  <Button
                    type="submit"
                    fullWidth
                    variant="outlined"
                    sx={{ mt: 3, mb: 2 }}
                    onClick={handleUpdateUser}
                    disabled={
                      formik.errors.fullname || formik.errors.age ? true : false
                    }
                  >
                    Submit
                  </Button>
                </Box>
              </Box>
            </Container>
          </ThemeProvider>
        }
      ></Screen>
    </>
  );
}
